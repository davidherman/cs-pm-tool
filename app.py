import openai
from brain import BrainInterface
from dotenv import load_dotenv
from flask import Flask, render_template
from flask_socketio import SocketIO, emit

openai.api_key = "your-api-key-here"
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


button_texts = [
    "What is the biggest issue at the moment?",
    "How was the communication with the client since I left? is there any negative email from them?",
    "Is there anything that negatively affects the delivery of the current sprint?",
    "Is there any change in the deadline or is anything behind the schedule?",
    "Who is behind their task the most right now? Do we know why?",
    "What is needed at the moment to deliver the next milestone on time? Is there anything we need to re-plan "
    "to achieve it?"
]

interface = BrainInterface()


@app.route('/')
def index():
    return render_template('index.html', button_texts=button_texts)


@socketio.on('message')
def handle_message(msg):
    ai_response = interface.ask_a_question(question=msg)
    emit('message', ai_response, broadcast=True)


if __name__ == '__main__':
    load_dotenv()
    socketio.run(app)

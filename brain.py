from langchain import OpenAI, ConversationChain, LLMChain, PromptTemplate
from langchain.memory import ConversationBufferWindowMemory
from langchain.chat_models import ChatOpenAI
import time


class BrainInterface:
    human_input_template = """From now, you are the project manager's assistant in the software development project, 
    your job is to know every details and status of the project. You have to pretend that you have source data 
    from JIRA and from the email. make up the necessary data.
    The project manager (PM), just came back from 5 days vacation and now have questions about the status
    You have direct access and direct integration to the project's JIRA, Slack, emails and google drive.
    The team is:
    - 3 BE developers
    - 2 FE developers
    - 2 Tester
    - 1 Analyst
    - 1 PM
    The project: to develop neo-bank phone application. The team is using agile methodology.
    - When you form your answers, and talking about issues, always recommend an actual actionable solution.
    - Limit your response to 300 characters.
    
    {history}
    PM: {human_input}
    Assistant:"""

    def __init__(self):
        human_input_prompt = PromptTemplate(
            input_variables=["history", "human_input"],
            template=self.human_input_template
        )

        model = "gpt-3.5-turbo"
        self.chatgpt_chain = LLMChain(
            llm=ChatOpenAI(model_name=model, temperature=0.6),
            prompt=human_input_prompt,
            verbose=True,
            memory=ConversationBufferWindowMemory(k=100),
        )

    def ask_a_question(self, question: str) -> str:
        start_time = time.time()
        response = self.chatgpt_chain.predict(human_input=question)
        end_time = time.time()
        execution_time = end_time - start_time
        return response
